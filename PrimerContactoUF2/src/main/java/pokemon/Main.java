package pokemon;

import java.util.List;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import pokemon.pokemon;

public class Main {


	static pokemon pokemon;
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry; 
		
		
		public static synchronized SessionFactory getSessionFactory() {
		    if ( sessionFactory == null ) {

		        // exception handling omitted for brevityaa

		        serviceRegistry = new StandardServiceRegistryBuilder()
		                .configure("hibernate.cfg.xml")
		                .build();

		        sessionFactory = new MetadataSources( serviceRegistry )
		                    .buildMetadata()
		                    .buildSessionFactory();
		    }
		    return sessionFactory;
		}
		
		
		public static void main(String[] args) {
			///abrimos sesion. Eso se hace siempre
			try {
				session = getSessionFactory().openSession();
				
				//abrimos PRIMERA transaccion. Eso se hace siempre.
				session.beginTransaction();
				
				Random r = new Random();
				
				
				for(int i = 200; i<210; i++) {
					pokemon pokemon = new pokemon();
					pokemon.setId(r.nextInt(800)+300);
					pokemon.getNombre();
					pokemon.setTipo1("Normal");
					pokemon.setTipo2("Fuego");
					pokemon.setGeneracion(1);
					
					
					session.saveOrUpdate(pokemon);
				}
				
				
				
				
				
				session.getTransaction().commit();
				
		        session.beginTransaction();
		         
		       
		       
		         session.getTransaction().commit();
				System.out.println("Va bien");
			} catch(Exception sqlException) {
				sqlException.printStackTrace();
				if(null != session.getTransaction()) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				sqlException.printStackTrace();
			} finally {
				if(session != null) {
					session.close();
				}
			}
			
			

		}

	}


