package pokemon;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pokemon")
public class pokemon 
{
@Id
@Column(name = "id")
private int id;
		
@Column(name = "nombre")
String nombre;
		
@Column(name = "tipo1")
String tipo1;
		
@Column(name = "tipo2")
String tipo2;
		
@Column(name = "generacion")
int generacion;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {	
	
	this.nombre = nombre;
}

public String getTipo1() {
	return tipo1;
}

public void setTipo1(String tipo1) {
	this.tipo1 = tipo1;
}

public String getTipo2() {
	return tipo2;
}

public void setTipo2(String tipo2) {
	this.tipo2 = tipo2;
}

public int getGeneracion() {
	return generacion;
}

public void setGeneracion(int generacion) {
	this.generacion = generacion;
}
		

}
