# Classificador de 3scenes mitjançant Deep Learning. Fa servir xarxes neuronals convolucionades

# import the necessary packages
from keras.models import Sequential
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras import optimizers
from keras.utils import to_categorical
from keras.optimizers import *
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from PIL import Image
from imutils import paths
import numpy as np
import argparse
import os

dataset = "dogsvscats"

# Agafa totes les imatges del dataset i les classifica
print("[INFO] loading images...")
imagePaths = paths.list_images(dataset)
data = []
labels = []

# bucle sobre les imatges
for imagePath in imagePaths:
	# carrega la imatge, la reescala a 32x32
	# remapeja l'intensistat del pixel del minim al maxim (per a que quedi mes diferenciada)
	image = Image.open(imagePath)
	image = np.array(image.resize((32, 32))) / 255.0
	data.append(image)

	# Treu l'etiqueta del path (es a dir, l'etiqueta es el nom de la carpeta)
	label = imagePath.split(os.path.sep)[-2]
	labels.append(label)

# codifica les etiquetes a enters
lb = LabelBinarizer()
labels = lb.fit_transform(labels)


# Carrega el dataset d'Iris i el divideix en dos datasets aleatoriament
# Fa servir 3/4 per training i 1/4 per avaluació
(trainX, testX, trainY, testY) = train_test_split(np.array(data),
	np.array(labels), test_size=0.25)


trainY = to_categorical(trainY)
testY = to_categorical(testY)


# defineix la CNN
model = Sequential()
#Primera capa, de 8. aquesta es una capa d'input a on li entren els 32*32 pixels de l'imatge
model.add(Conv2D(32, (3, 3), padding="same", input_shape=(32, 32, 3)))
#funcio d'activació. determina com de "segur" ha d'estar per a tornar 1.
model.add(Activation("relu"))
#El max pooling serveix per que la xarxa no quedi inmensa i tardi anys
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
#Segona capa, de 16, intermitja. el 3,3 es la mida del kernel (mesura cada pixel en relacio als pixels dels costats)
model.add(Conv2D(32, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(256, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(256, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Dropout(0.5))
model.add(Conv2D(128, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
#Afegeix una capa final de 3, amb un classificador softmax (donarà output i serà amb percentatges de probabilitat)
model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(2))
model.add(Activation("softmax"))
    


# Inicialitza l'optimitzador i model d'entrenament
print("[INFO] training network...")
# Adam es un bon optimitzador per CNN, pero no sempre el millor. https://keras.io/optimizers/
opt = Adam(lr=1e-3, decay=1e-3 / 50)
model.compile(loss="categorical_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# Entrenament. Aqui has d'indicar cuantes "epoques" (generacions) vols entrenar l'algoritme.
H = model.fit(trainX, trainY, validation_data=(testX, testY),
	epochs=20, batch_size=32)

# avaluació
print("[INFO] evaluating network...")
predictions = model.predict(testX, batch_size=32)
print(classification_report(testY.argmax(axis=1),
	predictions.argmax(axis=1), target_names=lb.classes_))
