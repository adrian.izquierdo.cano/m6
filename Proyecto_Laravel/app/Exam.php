<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function pregunta()
    {
        return $this->hasMany(Pregunta::class);
    }

    public function respuesta()
    {
        return $this->hasMany(Respuesta::class);
    }
}
