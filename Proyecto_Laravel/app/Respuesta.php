<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    public function exams()
    {
    	return $this->belongsTo(Exam::class);
    }
}
