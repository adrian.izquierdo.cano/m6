<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use App\Respuesta;
use App\Pregunta;
use App\Exam;


class ExamsController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function formCreate()
    {
        if(Auth::user()->rol == 'profesor')
        {
            return view('create');
        }
        else {
            return view('home');
        }        
    }
    public function create()
    {
        if(Auth::check())
        {
            if(Auth::user()->rol == 'profesor')
            {
                $preguntas = new Pregunta();

                $preguntas->modelo = request('modelo');
                $preguntas->enunciado = request('enunciado');
                $preguntas->respuesta = request('respuesta');
                $preguntas->puntuacion = 1;

                $count = DB::table('preguntas')->where('enunciado', $preguntas->enunciado)->where('modelo', $preguntas->modelo)->count();
                $numero = DB::table('preguntas')->where('modelo', $preguntas->modelo)->count();
                
                $preguntas->numero = $numero + 1;
                if($count > 0)
                {
                    DB::table('preguntas')->where('enunciado', $preguntas->enunciado)->where('modelo', $preguntas->modelo)->update(['respuesta' => $preguntas->respuesta]);
                }
                else 
                {
                    $preguntas->save(); 
                }
                return redirect('create');
            }
            else if(Auth::user()->rol == 'alumno')
            {
                return redirect('home');
            }
        }
        else {
            return redirect('login');
        }
    }

    public function showExam(Request $request,$examen)
    {
        if(Auth::check())
        {
            $userId = Auth::id();
            $intentos = DB::table('exams')->where('modelo', $examen)->where('user_id',$userId)->count();
            if($intentos >= 3)
            {
                return redirect('home');
            }
            else 
            {        
                $preguntas = DB::table('preguntas')->where('modelo', $examen)->orderBy('numero', 'asc')->get();
                return view('do')->with('preguntas',$preguntas)->with('examen',$examen);
            }
        }
    }

    public function do(Request $request, $examen)
    {
        if(Auth::check())
        {
            $exam = new Exam();
            
            $userId = Auth::id();
            $puntuacionTotal = 0;
            $intentos = DB::table('exams')->where('user_id', $userId)->where('modelo', $examen)->count();
            $numeroRespuesta = 0;
            $intentos = $intentos + 1;

            $answers = $request->input('respuesta');
            foreach ($answers as $answer)
            {
                $numeroRespuesta = $numeroRespuesta + 1;

                $respuesta = new Respuesta();
                $respuesta->user_id = $userId;
                $respuesta->numero = $numeroRespuesta;
                $respuesta->modelo = $examen;
                $respuesta->respuesta = $answer;
                $respuesta->intentos = $intentos;
                $enunciado = DB::table('preguntas')->select('enunciado')->where('numero', $numeroRespuesta)->where('modelo', $respuesta->modelo)->first();
                $respuesta->enunciado = $enunciado->enunciado;
                $count = DB::table('preguntas')->where('respuesta', $respuesta->respuesta)->where('modelo', $respuesta->modelo)->count();
                if($count > 0)
                {
                    $puntos = DB::table('preguntas')->where('respuesta', $respuesta->respuesta)->where('modelo', $respuesta->modelo)->first();
                    $respuesta->puntuacion = $puntos->puntuacion;
                }
                else {
                    $respuesta->puntuacion = 0;
                }
                $puntuacionTotal = $puntuacionTotal + $respuesta->puntuacion;
                $respuesta->save();
            }
            
            $exam->user_id = $userId;
            $exam->modelo = $examen;
            $exam->puntuacion = $puntuacionTotal;
            $exam->intentos = $intentos;
            $exam->save();

            return redirect('home');
        }
    }

    public function rank()
    {
        if(Auth::check())
        {
            $examenes = DB::table('exams')->orderBy('modelo', 'asc')->orderBy('puntuacion', 'desc')->get();

            return view('rank')->with('examenes',$examenes);
        }
    }
}
