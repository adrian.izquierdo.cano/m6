<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use App\Respuesta;
use App\Pregunta;
use App\Exam;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check())
        {
            if(Auth::user()->rol == 'profesor')
            {
                return view('create');
            }
            else if(Auth::user()->rol == 'alumno')
            {
                $userId = Auth::id();
                $examenes = DB::table('exams')->where('user_id', $userId)->get();
                $respuestas = DB::table('respuestas')->where('user_id', $userId)->get();

                return view('home')->with('examenes',$examenes)->with('respuestas',$respuestas);
            }
        }
        else {
            return view('login');
        }
    }
}
