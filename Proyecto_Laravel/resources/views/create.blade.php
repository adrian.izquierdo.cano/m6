@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Crea preguntas para el examen que elijas. Con este formulario creas examenes creando las preguntas una a una y escoges examen con el numero de examen
                    que quieres. Si el enunciado y el examen seleccionado coinciden con uno de los que ya esta en base de datos updatea la respuesta de este</p>
                    <form method="POST" action="/create">
                        {{ csrf_field() }}
                        <span>Modelo de examen en numero</span>
                        <input type="text" name="modelo"></br>
                        <span>Enunciado</span>
                        <input type="text" name="enunciado"></br>
                        <span>Respuesta</span>
                        <input type="text" name="respuesta"></br>
                        <input type="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
