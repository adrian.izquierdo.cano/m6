@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::user()->rol == 'alumno')
                    <form method="POST" action="/do/{{$examen}}">
                        {{ csrf_field() }}
                        @foreach ($preguntas as $pregunta)
                            <span>Pregunta: </span>
                            {{ $pregunta->enunciado }}
                            <input type='text' name="respuesta[]"></br>
					    @endforeach
						<input type='submit'>
                    </form>
                    @elseif(Auth::user()->rol == 'profesor')
                        @foreach ($preguntas as $pregunta)
                            <span>Pregunta: </span>
                            {{ $pregunta->enunciado }}</br>
					    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection