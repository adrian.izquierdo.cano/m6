@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Tus examenes</p>                
                    @foreach ($examenes as $examen)
                        <p>Datos examen</p>
                        <span>Modelo del examen: </span>
                        {{ $examen->modelo }}</br>
                        <span>Puntuación: </span>
						{{ $examen->puntuacion }}</br>
                        <span>Intento: </span>
						{{ $examen->intentos }}</br>
                        <p>Resultados:</p>
                        @foreach ($respuestas as $respuesta)
                            @if($examen->modelo == $respuesta->modelo && $examen->intentos == $respuesta->intentos)
                                <span>Pregunta: </span>
                                {{ $respuesta->numero }}
                                {{ $respuesta->enunciado }}</br>
                                <span>Respuesta: </span>
                                {{ $respuesta->respuesta }}
                                <span>Puntuacion</span>
                                {{ $respuesta->puntuacion }}</br>
                            @endif
                        @endforeach  
					@endforeach	
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection