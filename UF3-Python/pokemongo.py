import pymongo
import datetime
from random import randint
from pymongo import MongoClient

client = MongoClient()

db = client["project"]
coll = db["pokemon"]
team = db["team"]

while True:    
    key = input()
    lista = key.split()
    def primera_parte(lista):
        if lista[2] == "evol":
            evol_list = []
            if lista[1].isdigit():
                tiene_prev = coll.find_one({"num":lista[1],"prev_evolution":{"$exists":True}})
                tiene_evol = coll.find_one({"num":lista[1],"next_evolution":{"$exists":True}})
                doc = coll.find({"num":lista[1]})
                if tiene_prev is None and tiene_evol is None:
                    for i in doc:
                        actual = i["name"]
                        evol_list.append(actual)
                    print(evol_list)    
                elif tiene_prev is None and tiene_evol is not None:
                    for i in doc:
                        actual = i["name"]
                        evol_list.append(actual)
                        siguiente = i["next_evolution"]
                    for j in siguiente:
                        next_evol = j["name"]
                        evol_list.append(next_evol) 
                    print(evol_list)
                elif tiene_prev is not None and tiene_evol is not None:                   
                    for i in doc:
                        anterior = i["prev_evolution"]  
                        actual = i["name"]
                        siguiente = i["next_evolution"]
                    for j in anterior:
                        prev_evol = j["name"]
                        evol_list.append(prev_evol) 
                    evol_list.append(actual)    
                    for j in siguiente:
                        next_evol = j["name"]
                        evol_list.append(next_evol) 
                    print(evol_list)
                elif tiene_prev is not None and tiene_evol is None:
                    for i in doc:
                        actual = i["name"]
                        anterior = i["prev_evolution"]     
                    for j in anterior:
                        prev_evol = j["name"]
                        evol_list.append(prev_evol)     
                    evol_list.append(actual)
                    print(evol_list)
            else:
                tiene_prev = coll.find_one({"name":lista[1],"prev_evolution":{"$exists":True}})
                tiene_evol = coll.find_one({"name":lista[1],"next_evolution":{"$exists":True}})
                doc = coll.find({"name":lista[1]})
                if tiene_prev is None and tiene_evol is None:
                    for i in doc:
                        actual = i["name"]
                        evol_list.append(actual)
                    print(evol_list)    
                elif tiene_prev is None and tiene_evol is not None:
                    for i in doc:
                        actual = i["name"]
                        evol_list.append(actual)
                        siguiente = i["next_evolution"]
                    for j in siguiente:
                        next_evol = j["name"]
                        evol_list.append(next_evol) 
                    print(evol_list)
                elif tiene_prev is not None and tiene_evol is not None:                   
                    for i in doc:
                        anterior = i["prev_evolution"]  
                        actual = i["name"]
                        siguiente = i["next_evolution"]
                    for j in anterior:
                        prev_evol = j["name"]
                        evol_list.append(prev_evol) 
                    evol_list.append(actual)    
                    for j in siguiente:
                        next_evol = j["name"]
                        evol_list.append(next_evol) 
                    print(evol_list)
                elif tiene_prev is not None and tiene_evol is None:
                    for i in doc:
                        actual = i["name"]
                        anterior = i["prev_evolution"]     
                    for j in anterior:
                        prev_evol = j["name"]
                        evol_list.append(prev_evol)     
                    evol_list.append(actual)
                    print(evol_list)
        else:
            tupla = ()
            if lista[1].isdigit():
                doc = coll.find_one({"num":lista[1]})              
                nombre = doc["name"]
                tupla = tupla + (nombre,)
                for i in lista[2:]:
                    res = doc[i]
                    tupla = tupla + (res,)
                print(tupla)
            else:
                doc = coll.find_one({"name":lista[1]})              
                nombre = doc["name"]
                tupla = tupla + (nombre,)
                for i in lista[2:]:
                    res = doc[i]
                    tupla = tupla + (res,)
                print(tupla)

    def segunda_parte_catch(lista):
        now = datetime.datetime.now()
        date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
        rand = randint(1, 501)  
        if lista[1].isdigit():
            isInYourTeam = team.find_one({"num":lista[1]})
            doc = coll.find({"num":lista[1]})
            if isInYourTeam is None:
                for i in doc:
                    team.insert_one(i)
                    res = i["name"]
                team.update_one({'catch_date': {"$exists" : False}},{"$unset": {'id':"",'img':"",'type':"",'height':"",'weight':"",'egg':"",'spawn_chance':"",'avg_spawns':"",'spawn_time':"",'multipliers':"",'weaknesses':""}})   
                team.update_one({'catch_date': {"$exists" : False}}, {"$set": {'catch_date': date_time, 'CP':rand,'current_candy':0.0}})   
                howmany = team.count_documents({})
                print(res +" atrapado. Cantidad de Pokémons actual: " + str(howmany))
            else:
                print("Ya tienes este Pokémon")
        else:
            isInYourTeam = team.find_one({"name":lista[1]})
            doc = coll.find({"name":lista[1]})
            if isInYourTeam is None:
                for i in doc:
                    team.insert_one(i)
                    res = i["name"]
                team.update_one({'catch_date': {"$exists" : False}},{"$unset": {'id':"",'img':"",'type':"",'height':"",'weight':"",'egg':"",'spawn_chance':"",'avg_spawns':"",'spawn_time':"",'multipliers':"",'weaknesses':""}})   
                team.update_one({'catch_date': {"$exists" : False}}, {"$set": {'catch_date': date_time, 'CP':rand,'current_candy':0.0}})   
                howmany = team.count_documents({})
                print(res +" atrapado. Cantidad de Pokémons actual: " + str(howmany))
            else:
                print("Ya tienes este Pokémon")

    def segunda_parte_release(lista):
        if lista[1].isdigit():
            doc = team.find({"num":lista[1]})
            for i in doc:
                res = i["name"]
            deleted = team.find_one_and_delete({"num":lista[1]})
            if deleted is None:
                print("No tienes este Pokémon")
            else:       
                howmany = team.count_documents({})
                print(res + " Liberado. Cantidad de Pokémons actual: " + str(howmany))
        else:
            doc = team.find({"name":lista[1]})
            for i in doc:
                res = i["name"]
            deleted = team.find_one_and_delete({"name":lista[1]})
            if deleted is None:
                print("No tienes este Pokémon")
            else:       
                howmany = team.count_documents({})
                print(res + " Liberado. Cantidad de Pokémons actual: " + str(howmany))

    def tercera_parte(lista):
        if lista[1].isdigit():
            update = team.find_one_and_update({"num":lista[1]},{"$inc":{"current_candy":1}}, new = True)
            doc = team.find({"num":lista[1]})
            tiene_evol = team.find_one({"num":lista[1],"next_evolution":{"$exists":True}})
            if tiene_evol is not None:
                if update['current_candy'] >= update['candy_count']:
                    for i in doc:  
                        nombre = i["name"] 
                        num = i["next_evolution"][0]["num"]
                        cp = i["CP"] + 100
                        date = i["catch_date"]
                    team.delete_one({"num":lista[1]})
                    doc = coll.find({"num":num})
                    for i in doc:
                        team.insert_one(i)
                        res = i["name"]
                    team.update_one({'catch_date': {"$exists" : False}},{"$unset": {'id':"",'img':"",'type':"",'height':"",'weight':"",'egg':"",'spawn_chance':"",'avg_spawns':"",'spawn_time':"",'multipliers':"",'weaknesses':""}})   
                    team.update_one({'catch_date': {"$exists" : False}}, {"$set": {'catch_date': date, 'CP':cp,'current_candy':0.0}})   
                    print(nombre + " a evolucionado a " + res)
                else:
                    for i in doc:
                        nombre = i["name"]
                        cantidad_caramelos = i["current_candy"]
                    print("Uuuu un caramelo. " + nombre +" tiene " + str(cantidad_caramelos) +" caramelos actualmente.")
            else:
                print("No puedes cebar a este Pokémon")
        else:
            update = team.find_one_and_update({"name":lista[1]},{"$inc":{"current_candy":1}}, new = True)
            doc = team.find({"name":lista[1]})
            tiene_evol = team.find_one({"name":lista[1],"next_evolution":{"$exists":True}})
            if tiene_evol is not None:
                if update['current_candy'] >= update['candy_count']:
                    for i in doc:  
                        nombre = i["name"] 
                        num = i["next_evolution"][0]["num"]
                        cp = i["CP"] + 100
                        date = i["catch_date"]
                    team.delete_one({"name":lista[1]})
                    doc = coll.find({"num":num})
                    for i in doc:
                        team.insert_one(i)
                        res = i["name"]
                    team.update_one({'catch_date': {"$exists" : False}},{"$unset": {'id':"",'img':"",'type':"",'height':"",'weight':"",'egg':"",'spawn_chance':"",'avg_spawns':"",'spawn_time':"",'multipliers':"",'weaknesses':""}})   
                    team.update_one({'catch_date': {"$exists" : False}}, {"$set": {'catch_date': date, 'CP':cp,'current_candy':0.0}})   
                    print(nombre + " a evolucionado a " + res)
                else:
                    for i in doc:
                        nombre = i["name"]
                        cantidad_caramelos = i["current_candy"]
         
                    print("Uuuu un caramelo. " + nombre +" Tiene " + str(cantidad_caramelos) +" caramelos actualmente.")
            else:
                print("No puedes cebar a este Pokémon")
                

    def cuarta_parte(lista):
        mismo_tipo = []
        doc = coll.find({"type":lista[1]},{"name":1, "_id":0})
        howmany = coll.count_documents({"type":lista[1]})
        for i in doc:
            mismo_tipo.append(i["name"])
        print(mismo_tipo , howmany)
    
    orden = lista[0].lower()
    if orden == "search":
        primera_parte(lista)

    elif orden == "catch":
        segunda_parte_catch(lista)

    elif orden == "release":
        segunda_parte_release(lista)

    elif orden == "candy":
        tercera_parte(lista)

    elif orden == "type":
        cuarta_parte(lista)