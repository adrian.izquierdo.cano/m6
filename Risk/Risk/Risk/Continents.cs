﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Risk
{
    [Table("Continents")]
    class Continents
    {
        
        [Key]
        public int continentID { get; set; }
        [Required]
        public string continentName { get; set; }
        public int ownerId { get; set; }
        [Required]
        public int troopsBonus { get; set; }
        [Required]
        public List<Regions> continentRegions { get; set; }

        public Player Player { get; set; }

        public ICollection <Regions> Regions { get; set; }

    
    public Continents()
    {
            this.continentRegions = new List<Regions>();
    }
  }
}
