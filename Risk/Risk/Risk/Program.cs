﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Risk
{
    class Program
    {
        static RiskContext ctx;

        static void Main(string[] args)
        {

            ctx = new RiskContext();

            Continents c1 = new Continents() { continentName = "c1", troopsBonus = 5 };
            Continents c2 = new Continents() { continentName = "c2", troopsBonus = 5 };


            Regions region1 = new Regions() { regionName = "r1", ownerId = 1, troops = 5 };
            Regions region2 = new Regions() { regionName = "r2", ownerId = 2, troops = 5 };
            Regions region3 = new Regions() { regionName = "r3", ownerId = 1, troops = 5 };
            Regions region4 = new Regions() { regionName = "r4", ownerId = 2, troops = 5 };
            Regions region5 = new Regions() { regionName = "r5", ownerId = 1, troops = 5 };
            Regions region6 = new Regions() { regionName = "r6", ownerId = 2, troops = 5 };

            c1.continentRegions.Add(region1);
            c1.continentRegions.Add(region2);
            c1.continentRegions.Add(region3);
            c2.continentRegions.Add(region4);
            c2.continentRegions.Add(region5);
            c2.continentRegions.Add(region6);

            region1.neighbor.Add(region2);
            region1.neighbor.Add(region3);
            region2.neighbor.Add(region1);
            region2.neighbor.Add(region3);
            region2.neighbor.Add(region4);
            region2.neighbor.Add(region5);
            region3.neighbor.Add(region1);
            region3.neighbor.Add(region2);
            region3.neighbor.Add(region4);
            region3.neighbor.Add(region5);
            region4.neighbor.Add(region6);
            region4.neighbor.Add(region2);
            region4.neighbor.Add(region3);
            region4.neighbor.Add(region5);
            region5.neighbor.Add(region6);
            region5.neighbor.Add(region2);
            region5.neighbor.Add(region3);
            region5.neighbor.Add(region4);
            region6.neighbor.Add(region4);
            region6.neighbor.Add(region5);



            Player player1 = new Player() { name = "Player1", order = 1, ownedRegions = 0, contador = 0, vivo = true };
            Player player2 = new Player() { name = "Player2", order = 2, ownedRegions = 0, contador = 0, vivo = true };



            ctx.Player.Add(player1);
            ctx.Player.Add(player2);

            ctx.Regions.Add(region1);
            ctx.Regions.Add(region2);
            ctx.Regions.Add(region3);
            ctx.Regions.Add(region4);
            ctx.Regions.Add(region5);
            ctx.Regions.Add(region6);

            ctx.Continents.Add(c1);
            ctx.Continents.Add(c2);

            ctx.SaveChanges();
            /* try
             {

             }
             catch (DbEntityValidationException e)
             {
                 foreach (var eve in e.EntityValidationErrors)
                 {
                     Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                         eve.Entry.Entity.GetType().Name, eve.Entry.State);
                     foreach (var ve in eve.ValidationErrors)
                     {
                         Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                             ve.PropertyName, ve.ErrorMessage);
                     }
                 }
                 throw;
             }*/



            Play(ctx);
        }

        public static void Play(RiskContext ctx)
        {
            int order = 1;
            string jugando = "true";

            /*Start(ctx);*/

            while (jugando != "exit")
            {
                Turn(order, ctx);
                string continueatt = "S";
                while(continueatt == "S")
                {
                    Attack(order, ctx);
                    Console.WriteLine("Seguir atacando? (S/N)");
                    continueatt = Console.ReadLine();
                }
                Check(order, ctx);
                ReassignTroops(order, ctx);  
                order = ChangeOrder(order);

            }
        }

        /*public static void Start(RiskContext ctx)
        {
            List<Regions> listregions = ctx.Regions.ToList();

            foreach (Regions r in listregions)
            {

                if (r.regionName == "region1")
                {
                    r.ownerId = 1;
                    r.troops = 5;
                }
                if (r.regionName == "region2")
                {
                    r.ownerId = 2;
                    r.troops = 5;
                }
                if (r.regionName == "region3")
                {
                    r.ownerId = 1;
                    r.troops = 5;
                }
                if (r.regionName == "region4")
                {
                    r.ownerId = 2;
                    r.troops = 5;
                }
                if (r.regionName == "region5")
                {
                    r.ownerId = 1;
                    r.troops = 5;
                }
                if (r.regionName == "region6")
                {
                    r.ownerId = 2;
                    r.troops = 5;
                }
            }
  
        }*/

        public static void Turn(int order, RiskContext ctx)
        {
            List<Regions> listregions = ctx.Regions.ToList();
            List<Continents> listcontinents = ctx.Continents.ToList();

            foreach (Regions r in listregions)
            {
                if (r.ownerId == order)
                {
                    Console.WriteLine(r.regionName);

                }
            }
            Console.WriteLine("Selecciona donde añadir tropas");

            string regionSelected = Console.ReadLine();

            foreach (Regions r in listregions)
            {
                if (r.ownerId == order)
                {
                    int regionsOwned = ctx.Regions.Count();

                    if (regionSelected == r.regionName)
                    {
                        r.troops += 5;
                        if (regionsOwned >= 3)
                        {
                            r.troops++;

                        }

                        foreach (Continents c in listcontinents)
                        {
                            if (c.ownerId == order)
                            {
                                r.troops += c.troopsBonus;
                            }
                        }

                    }
                }

            }
            ctx.SaveChanges();
        }

        public static void Attack(int order, RiskContext ctx)
        {
            List<Regions> listregions = ctx.Regions.ToList();

            Console.WriteLine("Selecciona la region desde la que vas a atacar");
            foreach (Regions r in listregions)
            {
                if (r.ownerId == order)
                {
                    Console.WriteLine(r.regionName);
                }
                
            }

            string regionSelected = Console.ReadLine();



            Console.WriteLine("Selecciona la region a la que vas a atacar");

            foreach (Regions r in listregions)
            {
                if (r.regionName == regionSelected)
                {
                    var res = r.neighbor.Where(re => re.ownerId != order);
                    
                    // No acabo de saber como mostrar la lista bien
                    Console.WriteLine(res);
                }
            }

            string regionSelected1 = Console.ReadLine();

            Console.WriteLine("Con cuantas tropas atacas");
            int natk = Convert.ToInt32(Console.ReadLine());
            if(natk > 3)
            {
                natk = 3;
            }
            if(natk <= 0)
            {
                natk = 1;
            }

            Console.WriteLine("Con cuantas tropas defiendes");
            int ndef = Convert.ToInt32(Console.ReadLine());
            if (ndef > 2)
            {
                ndef = 2;
            }
            else
            {
                ndef = 1;
            }

            DiceRoll(natk,ndef, out int atkloss, out int defloss);

            foreach (Regions r in listregions)
            {
                if (r.regionName == regionSelected)
                {
                    r.troops -= atkloss;
                }
                if(r.regionName == regionSelected1)
                {
                    r.troops -= defloss;
                    if(r.troops <= 0)
                    {
                        r.ownerId = order;
                        r.troops = natk;
                        // No acabo de saber como optener el valor de la region seleccionada

                    }
                }

            }

            ctx.SaveChanges();

        }

       

        public static void ReassignTroops(int order, RiskContext ctx)
        {
            List<Regions> listregions = ctx.Regions.ToList();

            Console.WriteLine("Selecciona una region para mover tropas");

            foreach (Regions r in listregions)
            {
                if (r.ownerId == order)
                {
                    Console.WriteLine(r.regionName);
                }

            }
            string regionSelected = Console.ReadLine();

            Console.WriteLine("Selecciona la region donde mandas las tropas");

            foreach (Regions r in listregions)
            {
                if (r.ownerId == order)
                {
                    if(regionSelected == r.regionName)
                    {

                        var res = r.neighbor.Where(re => re.ownerId == order);
                        //Lo mismo de antes los vecinos no se mostrarlos correctamente
                        Console.WriteLine(res);
                    }
                    
                }

            }
            string regionSelected1 = Console.ReadLine();

            Console.WriteLine("Cuantos soldados");
            int troopsQuantiti = Convert.ToInt32(Console.ReadLine());

            foreach (Regions r in listregions)
            {
                if (r.ownerId == order)
                    if (regionSelected == r.regionName)
                    {
                        if (r.troops > 1)
                        {
                            r.troops -= troopsQuantiti;

                        }
                    }

                    if (regionSelected1 == r.regionName)
                    {

                        r.troops += troopsQuantiti;


                    }
            }

            
            ctx.SaveChanges();
        }

        public static void Check(int order, RiskContext ctx)
        {
            List<Regions> listregions = ctx.Regions.ToList();

            /*foreach (Regions r in listregions)
            {
                if(r.ownerId == order)
                {
                    if(ctx.Regions.Count() == 6)
                    {
                        Console.WriteLine(order + "Victory");
                    }
                    else
                    {
                        return;
                    }
                }
            }*/
        }

        public static int ChangeOrder(int order)
        {
            if(order == 1)
            {
                order = 2;                
            }
            if (order == 2)
            {
                order = 1;          
            }
            return order;
        }

        public static void DiceRoll(int natk, int ndef, out int lossatk, out int lossdef)
        {
            int[] atk = new int[natk];
            int[] def = new int[ndef];
            lossatk = 0;
            lossdef = 0;

            Random r = new Random();
            for (int i = 0; i < natk; i++)
            {
                atk[i] = r.Next(1, 7);
            }
            for (int i = 0; i < ndef; i++)
            {
                def[i] = r.Next(1, 7);
            }
            Array.Sort(atk);
            Array.Reverse(atk);
            Array.Sort(def);
            Array.Reverse(def);

            for (int i = 0; i < ndef && i < natk; i++)
            {
                if (atk[i] > def[i]) lossdef++;
                else lossatk++;
            }
        }
    }
}
