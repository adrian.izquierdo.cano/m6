﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Risk
{
    [Table("Regions")]
    class Regions
    {
        
        [Key]
        public int regionID { get; set; }
        [Required]
        public string regionName { get; set; }
        [Required]
        public int ownerId { get; set; }
        [Required]
        public int troops { get; set; }
        [Required]
        public List<Regions> neighbor { get; set; }



        public Player Player { get; set; }

        public virtual ICollection<Regions> ReverseRegionNeighbor { get; set; }


        public Continents Continents { get; set; }

        public Regions()
        {
            this.neighbor = new List<Regions>();
           
        }
    }
}
