﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Risk
{
    class RiskContext : DbContext
    {
        public RiskContext() : base("Risk")
        {

            /*Console.WriteLine("funciona");*/
        }

        public DbSet<Player> Player { get; set; }
        public DbSet<Regions> Regions { get; set; }
        public DbSet<Continents> Continents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<RiskContext>()); //update, pero crea la DB!
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RiskContext>());  //aquest seria un metode update pero si canvies el model (els POCOS) reinicia la DB
            Database.SetInitializer(new DropCreateDatabaseAlways<RiskContext>());  //equivaldria a un create

            /*modelBuilder.Entity<Modul>()
            .HasRequired<Curs>(m => m.Curs)
            .WithMany(c => c.Modul)
            .HasForeignKey<int>(m => m.elSeuCurs)
            .WillCascadeOnDelete(false);
            */


            base.OnModelCreating(modelBuilder);


        }
    }
}
