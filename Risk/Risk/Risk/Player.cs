﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Risk
{
    [Table("Player")]
    class Player
    {
  
        [Key]
        public int playerID { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public int order { get; set; }
        [Required]
        public int ownedRegions { get; set; }
        [Required]
        public int contador { get; set; }
        [Required]
        public bool vivo { get; set; }

        public ICollection<Regions> Regions { get; set; }
        public ICollection<Continents> Continents { get; set; }

       
    }
}
